package ru.aplana.vingenerator.controller;

import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import ru.aplana.vingenerator.domain.VINCode;
import ru.aplana.vingenerator.generator.GeneratorService;
import ru.aplana.vingenerator.generator.implRule.LocationData;
import ru.aplana.vingenerator.repos.VINrepository;

import java.util.Collections;
import java.util.Map;
import java.util.stream.StreamSupport;

@Controller
public class WebGeneratorController {

    @Autowired
    GeneratorService generatorService;

    @Autowired
    VINrepository vinRepository;

    @GetMapping
    public String defaultPage() {
        return "redirect:/main";
    }

    @GetMapping("/main")
    public String main() {
        return "main";
    }

    @PostMapping("/main")
    public String genarateVinInitially() {
        return "redirect:/generator";
    }

    @GetMapping("/generator")
    public String generator(Map<String, Object> model) {
        //Берем последний сгенеренный vin
        VINCode lastVIN = StreamSupport
                .stream(vinRepository.findAll().spliterator(), false)
                .min(Collections.reverseOrder())
                .orElse(null);
        if (lastVIN != null) {
            model.put("code", lastVIN.getVINcode());
            model.put("location", lastVIN.getLocation());
        }
        return "generator";
    }

    @PostMapping("/generator")
    public String generateVIN(String location, Map<String, Object> model) {
        try {
            VINCode vinCode = new VINCode(generatorService.generateVINCode(location), location);
            if (vinCode.getLocation().isEmpty()) {
                vinCode.setLocation(LocationData.getLocationByCode(vinCode.getVINcode()));
            }
            vinRepository.save(vinCode);
            model.put("code", vinCode.getVINcode());
            model.put("location", vinCode.getLocation());
        } catch (NotFoundException ex) {
            model.put("message", ex.getMessage());
        }

        Iterable<VINCode> codes = StreamSupport
                .stream(vinRepository.findAll().spliterator(), false)
                .sorted(Collections.reverseOrder())
                .skip(1)::iterator;
        model.put("codes", codes);
        return "generator";
    }
}
