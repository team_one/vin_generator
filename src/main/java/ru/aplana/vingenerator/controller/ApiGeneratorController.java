package ru.aplana.vingenerator.controller;

import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.aplana.vingenerator.domain.VINCode;
import ru.aplana.vingenerator.generator.GeneratorService;
import ru.aplana.vingenerator.generator.implRule.LocationData;
import ru.aplana.vingenerator.repos.VINrepository;
import ru.aplana.vingenerator.response.BaseResponse;
import ru.aplana.vingenerator.response.VINCodeResponse;

import java.util.Collections;
import java.util.stream.StreamSupport;

@RestController
@RequestMapping("/api")
public class ApiGeneratorController {

    @Autowired
    GeneratorService generatorService;

    @Autowired
    VINrepository vinRepository;

    @GetMapping(value = "/generator")
    public ResponseEntity<VINCodeResponse> getVINcode() {

        VINCodeResponse response;
        HttpStatus status = HttpStatus.OK;
        VINCode lastVIN;

        //Берем последний сгенеренный vin
        lastVIN = StreamSupport
                .stream(vinRepository.findAll().spliterator(), false)
                .min(Collections.reverseOrder())
                .orElse(null);

        //Проверяем, что если нет записанных значений возвращаем NO_CONTENT.
        if (lastVIN == null) {
            status = HttpStatus.NO_CONTENT;
            response = new VINCodeResponse("", "");
        } else {
            response = new VINCodeResponse(lastVIN.getVINcode(), lastVIN.getLocation());
        }

        return new ResponseEntity<VINCodeResponse>(response, status);
    }

    @PostMapping(value = "/generator")
    public ResponseEntity<VINCodeResponse> generateVIN(String location) {
        VINCodeResponse response = null;
        HttpStatus status = HttpStatus.CREATED;

        //Генерируем код, формируем ответ, записываем значение в бд
        try {
            String generatedCode = generatorService.generateVINCode(location);
            if (location != null) {
                response = new VINCodeResponse(generatedCode, location);
            } else {
                location = LocationData.getLocationByCode(generatedCode);
                response = new VINCodeResponse(generatedCode, location);
            }
            vinRepository.save(new VINCode(generatedCode, location));
        } catch (NotFoundException ex) {
            status = HttpStatus.I_AM_A_TEAPOT;
            response = new VINCodeResponse("", "");
        }
        return new ResponseEntity<VINCodeResponse>(response, status);
    }

    @GetMapping
    public ResponseEntity<BaseResponse> showStatus() {
        BaseResponse response = new BaseResponse("success");
        return new ResponseEntity<BaseResponse>(response, HttpStatus.OK);
    }
}
