package ru.aplana.vingenerator.generator.ruleApi;

public interface ApplyRule {

    public String apply(String target);
}
