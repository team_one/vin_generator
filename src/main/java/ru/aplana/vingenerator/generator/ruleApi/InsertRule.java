package ru.aplana.vingenerator.generator.ruleApi;

import javassist.NotFoundException;

public interface InsertRule {

    public String insert() throws NotFoundException;
}
