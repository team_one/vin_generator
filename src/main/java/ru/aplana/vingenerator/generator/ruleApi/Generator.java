package ru.aplana.vingenerator.generator.ruleApi;

import javassist.NotFoundException;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

public class Generator {

    private Generator(String mask, List<InsertRule> insertRules, List<ApplyRule> applyRules) {
        if (mask.isEmpty()){
            throw new IllegalArgumentException("Маска не может быть пустой");
        }
        if (insertRules.isEmpty()){
            throw new IllegalArgumentException("Должно быть как минимум одно правило вставки");
        }
        int numUniqChars = countUniqueCharacters(mask);
        int numRules = insertRules.size() + applyRules.size();
        if (numUniqChars != numRules) {
            throw new IllegalArgumentException("Количество уникальных символов в маске должно соответствовать количеству правил");
        }
        this.mask = mask;
        this.insertRules = insertRules;
        this.applyRules = applyRules;
    }

    private final String mask;

    private final List<InsertRule> insertRules;

    private final List<ApplyRule> applyRules;

    public String getString() throws NotFoundException {

        char[] maskAsChars = mask.toCharArray();

        // список содержит набор уникальных символов из маски
        List<Character> uniqCharsInMask = new ArrayList<>();
        for (int i = 0; i < maskAsChars.length; i++) {
            if (!uniqCharsInMask.contains(maskAsChars[i])) {
                uniqCharsInMask.add(maskAsChars[i]);
            }
        }

        // отсортируем список уникальных символов лексиграфически
        //  сначала цифры, потом заглавные буквы, потом строчные
        uniqCharsInMask.sort(new Comparator<Character>() {
            @Override
            public int compare(Character o1, Character o2) {
                return o1 - o2;
            }
        });
        List<String> insertedStrFromRule = new ArrayList<>();
        List<List<Integer>> charPositionInRule = new ArrayList<>();
        for (int i = 0; i < uniqCharsInMask.size() - applyRules.size(); i++) {
            List<Integer> charPositionInMask = new ArrayList<>();
            int count = 0;
            for (int j = 0; j < maskAsChars.length; j++) {
                if (maskAsChars[j] == uniqCharsInMask.get(i)){
                    count++;
                    charPositionInMask.add(j);
                }
            }
            String temp = insertRules.get(i).insert();
            insertedStrFromRule.add(temp);
            if (count != temp.length()){
                throw new IllegalArgumentException("Количество символов в маске для правила должно соответствовать количеству символов генерируемых правилом");
            }
            charPositionInRule.add(charPositionInMask);
        }

        char[] result = new char[maskAsChars.length];


        for (int i = 0; i < insertedStrFromRule.size(); i++) {
            char[] array = insertedStrFromRule.get(i).toCharArray();
            List<Integer> listInt = charPositionInRule.get(i);
            for (int j = 0; j < array.length; j++) {
                int k = listInt.get(j);
                result[k] = array[j];
            }
        }

        String resultApply = new String(result);
        if (applyRules.isEmpty()) {
            return resultApply;
        }
        for (int i = 0; i < applyRules.size(); i++) {
            resultApply = applyRules.get(i).apply(resultApply);
        }
        return resultApply;
    }

    private int countUniqueCharacters(String input) {
        String unique = input.replaceAll("(.)(?=.*?\\1)", "");
        return unique.length();
    }

    public static class Builder {

        public Builder() {
            // private constructor
        }

        private String nestedMask;

        private List<InsertRule> nestedInsertRules = new ArrayList<>(4);

        private List<ApplyRule> nestedApplyRules = new ArrayList<>(2);

        public Builder setMask(String mask) {
            this.nestedMask = Objects.requireNonNull(mask);
            return this;
        }

        public Builder addInsertRule(InsertRule insertRule) {
            this.nestedInsertRules.add(Objects.requireNonNull(insertRule));
            return this;
        }

        public Builder addApplyRule(ApplyRule applyRule) {
            this.nestedApplyRules.add(Objects.requireNonNull(applyRule));
            return this;
        }

        public Generator build() {
            return new Generator(nestedMask, nestedInsertRules, nestedApplyRules);
        }

    }
}
