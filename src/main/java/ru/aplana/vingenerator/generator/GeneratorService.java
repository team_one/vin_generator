package ru.aplana.vingenerator.generator;

import javassist.NotFoundException;
import ru.aplana.vingenerator.generator.implRule.CheckSumRule;
import ru.aplana.vingenerator.generator.implRule.EndRule;
import ru.aplana.vingenerator.generator.implRule.FillerRule;
import ru.aplana.vingenerator.generator.implRule.PrefixRule;
import ru.aplana.vingenerator.generator.ruleApi.Generator;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service("generatorService")
public class GeneratorService {

    public String generateVINCode(String country) throws NotFoundException {
        Generator gen = new Generator.Builder()
                .setMask("AAAAAAAAXABCCCCCC")
                .addInsertRule(new PrefixRule(country))
                .addInsertRule(new FillerRule())
                .addInsertRule(new EndRule())
                .addApplyRule(new CheckSumRule())
                .build();

        return gen.getString();
    }
}
