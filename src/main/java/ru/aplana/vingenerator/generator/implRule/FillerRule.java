package ru.aplana.vingenerator.generator.implRule;

import ru.aplana.vingenerator.generator.ruleApi.InsertRule;
import ru.aplana.vingenerator.generator.Utils;

public class FillerRule implements InsertRule {

    private static final String ALPHABET = AlphabetData.VIN_ALPHABET.toString();

    @Override
    public String insert() {
        return Utils.randomString(ALPHABET, 1);
    }
}
