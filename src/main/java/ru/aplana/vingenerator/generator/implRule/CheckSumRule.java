package ru.aplana.vingenerator.generator.implRule;

import ru.aplana.vingenerator.generator.ruleApi.ApplyRule;

import java.util.HashMap;
import java.util.Map;

public class CheckSumRule implements ApplyRule {

    private Map<Integer, Integer> mapPositionWeights;
    private Map<Character, Integer> mapCharWeights;

//    от 0 до 16
//    Position	1	2	3	4	5	6	7	8	9	10	11	12	13	14	15	16	17
//    Weight	8	7	6	5	4	3	2	10	0	9	8	7	6	5	4	3	2
    private Map<Integer, Integer> populatePositionWeights() {
        Map<Integer, Integer> map = new HashMap<>();
        int[] weights = {8, 7, 6, 5, 4, 3, 2, 10, 0, 9, 8, 7, 6, 5, 4, 3, 2};
        for (int i = 0; i < weights.length; i++) {
            map.put(i, weights[i]);
        }
        return map;
    }

//    A: 1	B: 2	C: 3	D: 4	E: 5	F: 6	G: 7	H: 8	N/A
//    J: 1	K: 2	L: 3	M: 4	N: 5	N/A	P: 7	N/A	R: 9
//    S: 2	T: 3	U: 4	V: 5	W: 6	X: 7	Y: 8	Z: 9
//    1: 1  2: 2...
    private Map<Character, Integer> populateCharWeights() {
        Map<Character, Integer> map = new HashMap<>();
        char[] chars = AlphabetData.VIN_ALPHABET.toString().toCharArray();
        int[] weights = {1, 2, 3, 4, 5, 6, 7, 8, 1, 2, 3, 4, 5, 7, 9, 2, 3, 4, 5, 6, 7, 8, 9,
                0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        for (int i = 0; i < chars.length; i++) {
            map.put(chars[i], weights[i]);
        }
        return map;
    }

    private String getCheckSum(String target, Map<Integer, Integer> posW, Map<Character, Integer> chW) {
        char[] targetAsCharArray = target.toCharArray();
        int sum = 0, reminder = 0;
        for (int i = 0; i < posW.size(); i++) {
            if (targetAsCharArray[i] == '\u0000'){
                continue;
            }
            sum += posW.get(i) * chW.get(targetAsCharArray[i]);
        }
        reminder = sum % 11;
        if (reminder == 10) { // в случае если остаток от деления =10 следует вставить на место контрольной суммы символ "X"
            return "X";
        }
        return Integer.toString(reminder);
    }

    @Override
    public String apply(String target) {
        if (mapPositionWeights == null & mapCharWeights == null) {
            mapPositionWeights = populatePositionWeights();
            mapCharWeights = populateCharWeights();
        }
        getCheckSum(target, mapPositionWeights, mapCharWeights);
        char[] charSeq = target.toCharArray();

        charSeq[8] = getCheckSum(target, mapPositionWeights, mapCharWeights).charAt(0);
        return new String(charSeq);
    }
}
