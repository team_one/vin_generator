package ru.aplana.vingenerator.generator.implRule;

public enum AlphabetData {

    VIN_ALPHABET("ABCDEFGHJKLMNPRSTUVWXYZ0123456789"),
    NUM_ALPHABET("0123456789");

    private String value;
    AlphabetData(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
