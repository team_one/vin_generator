package ru.aplana.vingenerator.generator.implRule;

import javassist.NotFoundException;
import org.springframework.data.crossstore.ChangeSetPersister;
import ru.aplana.vingenerator.generator.Utils;
import ru.aplana.vingenerator.generator.ruleApi.InsertRule;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PrefixRule implements InsertRule {

    private String location = null;

    public PrefixRule(){
    }

    public PrefixRule(String location){
        this.location = location;
    }


    @Override
    public String insert() throws NotFoundException {
        String result = "";

        if(location == null || location.isEmpty()){
            result = getRandomPrefix();
        } else {
            result = getCountryPrefix();
        }

        return result;
    }

    private String getRandomPrefix(){
        String result = "";

        int i = Utils.getRandNum(PrefixStash.getMap().size());
        for (String str: PrefixStash.getMap().get(i)) {
            result += str;
        }

        return  result;
    }

    private String getCountryPrefix() throws NotFoundException {
        String result = "";

        if(LocationData.countries.containsKey(location)){
            result = searchLocation(LocationData.countries);
        } else if (LocationData.continents.containsKey(location))
        {
            result = searchLocation(LocationData.continents);
        } else {
            throw new NotFoundException("Не найдена страна/континент - " + location + ".");
        }

        return  result;
    }

    private String searchLocation(HashMap<String, List<String>> searchedList) throws NotFoundException {
        String result = "";
        List<String> countryPrefixes =  searchedList.get(location);
        List<Integer> indexesOnPrefixStash = new ArrayList<>();
        for(int i = 0; i < PrefixStash.getMap().size(); i++)
        {
            for(int j = 0; j < countryPrefixes.size(); j++ ){
                if(PrefixStash.getMap().get(i).get(0).startsWith(countryPrefixes.get(j)))
                {
                    indexesOnPrefixStash.add(i);
                    break;
                }
            }
        }
        if(indexesOnPrefixStash.size() != 0){
            int i = Utils.getRandNum(indexesOnPrefixStash.size());
            for (String str: PrefixStash.getMap().get(indexesOnPrefixStash.get(i))) {
                result += str;
            }
        }
        else{
            throw new NotFoundException("Не найдены VIN по стране/континенту - " + location + ".");
        }

        return result;
    }
}
