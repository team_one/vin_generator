package ru.aplana.vingenerator.generator.implRule;

import ru.aplana.vingenerator.generator.ruleApi.InsertRule;
import ru.aplana.vingenerator.generator.Utils;

public class EndRule implements InsertRule {

    private static final String ALPHABET = AlphabetData.NUM_ALPHABET.toString();

    @Override
    public String insert() {
        return Utils.randomString(ALPHABET, 6);
    }
}
