package ru.aplana.vingenerator.generator.implRule;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LocationData {

    private static final String ALPHABET = AlphabetData.VIN_ALPHABET.toString();

    public static HashMap<String, List<String>> countries = initializeCountries();

    public static HashMap<String, List<String>> continents = initializeContinents();

    public static String getLocationByCode(String vinCode) {
        String prefixGenerated = vinCode.substring(0, 2);
        for (Map.Entry<String, List<String>> entry: LocationData.countries.entrySet()){
            for (String prefixCountry : entry.getValue()) {
                if(prefixCountry.equals(prefixGenerated)){
                    return entry.getKey();
                }
            }
        }
        return "";
    }

    private static void addRange(HashMap<String, List<String>> fillMap, char first, char start, char end, String name){
        char[] alphabet =  ALPHABET.toCharArray();
        int startIndex = ALPHABET.indexOf(start);
        int endIndex = ALPHABET.indexOf(end);

        List<String> prefixes = new ArrayList<>();
        if(first != Character.MIN_VALUE) {
            for (int i = startIndex; i <= endIndex; i++) {
                prefixes.add(new String(new char[]{first, alphabet[i]}));
            }
        } else{
            for (int i = startIndex; i <= endIndex; i++) {
                prefixes.add(new String(new char[]{alphabet[i]}));
            }
        }

        if(fillMap.containsKey(name)){
            fillMap.get(name).addAll(prefixes);
        }else{
            fillMap.put(name, prefixes);
        }
    }

    private static HashMap<String, List<String>> initializeCountries(){
        HashMap<String, List<String>> newCountries = new HashMap<>();

        // Africa
        addRange(newCountries, 'A', 'A','H',"South Africa");
        addRange(newCountries, 'A', 'J','N',"Ivory Coast");
        addRange(newCountries, 'B', 'A','E',"Angola");
        addRange(newCountries, 'B', 'F','K',"Kenya");
        addRange(newCountries, 'B', 'L','R',"Tanzania");
        addRange(newCountries, 'C', 'A','E',"Benin");
        addRange(newCountries, 'C', 'F','K',"Madagascar");
        addRange(newCountries, 'C', 'L','R',"Tunisia");
        addRange(newCountries, 'D', 'A','E',"Egypt");
        addRange(newCountries, 'D', 'F','K',"Morocco");
        addRange(newCountries, 'D', 'L','R',"Zambia");
        addRange(newCountries, 'E', 'A','E',"Ethiopia");
        addRange(newCountries, 'E', 'F','K',"Mozambique");
        addRange(newCountries, 'F', 'A','E',"Ghana");
        addRange(newCountries, 'F', 'F','K',"Nigeria");

        // AsianewCountries,
        addRange(newCountries, 'J', 'A','Z',"Japan");
        addRange(newCountries, 'J', '0','9',"Japan");
        addRange(newCountries, 'K', 'A','E',"Sri Lanka");
        addRange(newCountries, 'K', 'F','K',"Israel");
        addRange(newCountries, 'K', 'L','R',"Korea");
        addRange(newCountries, 'K', 'L','R',"Korea (South)");
        addRange(newCountries, 'K', 'S','Z',"Kazakhstan");
        addRange(newCountries, 'K', '0','0',"Kazakhstan");
        addRange(newCountries, 'L', 'A','Z',"China");
        addRange(newCountries, 'L', '0','9',"China");
        addRange(newCountries, 'M', 'A','E',"India");
        addRange(newCountries, 'M', 'F','K',"Indonesia");
        addRange(newCountries, 'M', 'L','R',"Thailand");
        addRange(newCountries, 'N', 'A','E',"Iran");
        addRange(newCountries, 'N', 'F','K',"Pakistan");
        addRange(newCountries, 'N', 'L','R',"Turkey");
        addRange(newCountries, 'N', 'L','R',"Turkey");
        addRange(newCountries, 'P', 'A','E',"Philippines");
        addRange(newCountries, 'P', 'F','K',"Singapore");
        addRange(newCountries, 'P', 'L','R',"Malaysia");
        addRange(newCountries, 'R', 'A','E',"United Arab Emirates");
        addRange(newCountries, 'R', 'F','K',"Taiwan");
        addRange(newCountries, 'R', 'L','R',"Vietnam");
        addRange(newCountries, 'R', 'S','Z',"Saudi Arabia");
        addRange(newCountries, 'R', '0','9',"Saudi Arabia");

        // EuropenewCountries,
        addRange(newCountries, 'S', 'A','M',"United Kingdom");
        addRange(newCountries, 'S', 'N','T',"East Germany");
        addRange(newCountries, 'S', 'U','Z',"Poland");
        addRange(newCountries, 'S', '1','4',"Latvia");
        addRange(newCountries, 'T', 'A','H',"Switzerland");
        addRange(newCountries, 'T', 'J','P',"Czech Republic");
        addRange(newCountries, 'T', 'R','V',"Hungary");
        addRange(newCountries, 'T', 'W','Z',"Portugal");
        addRange(newCountries, 'T', '1','1',"Portugal");
        addRange(newCountries, 'U', 'H','M',"Denmark");
        addRange(newCountries, 'U', 'N','T',"Ireland");
        addRange(newCountries, 'U', 'U','Z',"Romania");
        addRange(newCountries, 'U', '5','7',"Slovakia");
        addRange(newCountries, 'V', 'A','E',"Austria");
        addRange(newCountries, 'V', 'F','R',"France");
        addRange(newCountries, 'V', 'S','W',"Spain");
        addRange(newCountries, 'V', 'X','Z',"Serbia");
        addRange(newCountries, 'V', '1','2',"Serbia");
        addRange(newCountries, 'V', '3','5',"Croatia");
        addRange(newCountries, 'V', '6','9',"Estonia");
        addRange(newCountries, 'V', '0','0',"Estonia");
        addRange(newCountries, 'W', 'A','Z',"West Germany");
        addRange(newCountries, 'W', '0','9',"West Germany");
        addRange(newCountries, 'X', 'A','E',"Bulgaria");
        addRange(newCountries, 'X', 'F','K',"Greece");
        addRange(newCountries, 'X', 'L','R',"Netherlands");
        addRange(newCountries, 'X', 'S','W',"USSR");
        addRange(newCountries, 'X', 'X','Z',"Luxembourg");
        addRange(newCountries, 'X', '1','2',"Luxembourg");
        addRange(newCountries, 'X', '3','9',"Russia");
        addRange(newCountries, 'X', '0','0',"Russia");
        addRange(newCountries, 'Y', 'A','E',"Belgium");
        addRange(newCountries, 'Y', 'F','K',"Finland");
        addRange(newCountries, 'Y', 'L','R',"Malta");
        addRange(newCountries, 'Y', 'S','W',"Sweden");
        addRange(newCountries, 'Y', 'X','Z',"Norway");
        addRange(newCountries, 'Y', '1','2',"Norway");
        addRange(newCountries, 'Y', '3','5',"Belarus");
        addRange(newCountries, 'Y', '6','9',"Ukraine");
        addRange(newCountries, 'Y', '0','0',"Ukraine");
        addRange(newCountries, 'Z', 'A','R',"Italy");
        addRange(newCountries, 'Z', 'X','Z',"Slovenia");
        addRange(newCountries, 'Z', '1','2',"Slovenia");
        addRange(newCountries, 'Z', '3','5',"Lithuania");
        addRange(newCountries, 'Z', '3','5',"Lithuania");

        // North AmericanewCountries,
        addRange(newCountries, '1', 'A','Z',"United States");
        addRange(newCountries, '1', '0','9',"United States");
        addRange(newCountries, '2', 'A','Z',"Canada");
        addRange(newCountries, '2', '0','9',"Canada");
        addRange(newCountries, '3', 'A','Z',"Mexico");
        addRange(newCountries, '3', '1','7',"Mexico");
        addRange(newCountries, '3', '8','9',"Cayman Islands");
        addRange(newCountries, '3', '0','0',"Cayman Islands");
        addRange(newCountries, '4', 'A','Z',"United States");
        addRange(newCountries, '4', '0','9',"United States");
        addRange(newCountries, '5', 'A','Z',"United States");
        addRange(newCountries, '5', '0','9',"United States");

        // OceanianewCountries,
        addRange(newCountries, '6', 'A','W',"Australia");
        addRange(newCountries, '7', 'A','E',"New Zealand");

        // South AmericanewCountries,
        addRange(newCountries, '8', 'A','E',"Argentina");
        addRange(newCountries, '8', 'F','K',"Chile");
        addRange(newCountries, '8', 'L','R',"Ecuador");
        addRange(newCountries, '8', 'S','W',"Peru");
        addRange(newCountries, '8', 'X','Z',"Venezuela");
        addRange(newCountries, '8', '1','2',"Venezuela");
        addRange(newCountries, '9', 'A','E',"Brazil");
        addRange(newCountries, '9', 'F','K',"Colombia");
        addRange(newCountries, '9', 'L','R',"Paraguay");
        addRange(newCountries, '9', 'S','W',"Uruguay");
        addRange(newCountries, '9', 'X','Z',"Trinidad & Tobago");
        addRange(newCountries, '9', '1','2',"Trinidad & Tobago");
        addRange(newCountries, '9', '3','9',"Brazil");

        return newCountries;
    }

    private static HashMap<String, List<String>> initializeContinents(){
        HashMap<String, List<String>> newContinents = new HashMap<>();

        // Africa
        addRange(newContinents, Character.MIN_VALUE, 'A','H',"Africa");

        // Asia
        addRange(newContinents, Character.MIN_VALUE, 'J','R',"Asia");

        // Europe
        addRange(newContinents, Character.MIN_VALUE, 'S','Z',"Europe");

        // North America
        addRange(newContinents, Character.MIN_VALUE, '1','5',"North America");

        // Oceania
        addRange(newContinents, Character.MIN_VALUE, '6','7',"Oceania");

        // South America
        addRange(newContinents, Character.MIN_VALUE, '0','0',"South America");
        addRange(newContinents, Character.MIN_VALUE, '8','9',"South America");

        return newContinents;
    }
}
