package ru.aplana.vingenerator.generator.implRule;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PrefixStash {

    public PrefixStash() {

    }

    private static Map<Integer, List<String>> mapPrefixYear = new HashMap<>();

//    @Value("${path.vin.prefixes}")
//    private static String path;

    public static Map<Integer, List<String>> getMap() {
        return mapPrefixYear;
    }

    static {

        String path = "./files/VinPrefixes.txt";
        InputStream loader = ClassLoader.getSystemResourceAsStream(path);
        Pattern r = Pattern.compile("\\w+");
        List<String> tempList;
        if (loader != null) {
            try (BufferedReader br =
                         new BufferedReader(new InputStreamReader(loader, StandardCharsets.UTF_8))) {
                String line;
                int i = 0;
                while ((line = br.readLine()) != null) {
                    tempList = new ArrayList<>();
                    Matcher m = r.matcher(line);
                    // файл содержит строки следующего формата, по регексу делятся на две строки и записываются в список
                    // 137DA903   T
                    while (m.find()){
                        tempList.add(m.group());
                    }
                    mapPrefixYear.put(i, tempList);
                    i++;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
