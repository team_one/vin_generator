package ru.aplana.vingenerator.generator;

import java.security.SecureRandom;
import java.util.Random;

public class Utils {

    public static int getRandNum(int max) {
        return getRandNumRange(0, max);

    }

    public static int getRandNumRange(int min, int max) {
        if (min >= max) {
            throw new IllegalArgumentException("Верхняя граница должна быть больше нижней");
        }

        Random r = new Random();
        return r.nextInt(max - min);
    }

    public static String randomString(String alphabet, int length){
        if (0 >= length) {
            throw new IllegalArgumentException("Размер строки должен быть больше нуля");
        }

        char[] characterSet = alphabet.toCharArray();
        Random random = new SecureRandom();
        char[] result = new char[length];
        for(int i = 0; i < result.length; i++){
            int randomIndex = random.nextInt(characterSet.length);
            result[i] = characterSet[randomIndex];
        }
        return new String(result);
    }
}