package ru.aplana.vingenerator.repos;

import ru.aplana.vingenerator.domain.VINCode;
import org.springframework.data.repository.CrudRepository;

// CRUD refers Create, Read, Update, Delete
// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository

public interface VINrepository extends CrudRepository<VINCode, Integer>{

}
