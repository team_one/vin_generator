package ru.aplana.vingenerator.response;

public class VINCodeResponse {

    private final String vin;
    private final String location;

    public VINCodeResponse(String vin, String location){
        this.vin = vin;
        this.location = location;
    }

    public String getVin() {
        return this.vin;
    }

    public String getLocation() {
        return location;
    }
}
