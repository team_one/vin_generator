package ru.aplana.vingenerator.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class VINCode implements Comparable<VINCode> {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Integer id;

    private String VINcode;
    private String location;

    public VINCode() {}

    public VINCode(String VINcode, String location) {
        this.VINcode = VINcode;
        this.location = location;
    }

    @Override
    public int compareTo(VINCode vinCode) {
        return Integer.compare(id, vinCode.id);
    }

    public Integer getId() {
        return this.id;
    }

    public String getVINcode() {
        return this.VINcode;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setVINcode(String VINcode) {
        this.VINcode = VINcode;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}
